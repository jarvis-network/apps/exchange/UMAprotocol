const path = require("path");
const wkdir = path.dirname(require.resolve("@jarvis-network/uma-core/package.json"));

module.exports = require("@jarvis-network/uma-common").getTruffleConfig(wkdir);
