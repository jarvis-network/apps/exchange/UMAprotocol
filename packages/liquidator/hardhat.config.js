const { getHardhatConfig } = require("@jarvis-network/uma-common");

const path = require("path");
const coreWkdir = path.dirname(require.resolve("@jarvis-network/uma-core/package.json"));
const packageWkdir = path.dirname(require.resolve("@jarvis-network/uma-liquidator/package.json"));

const configOverride = {
  paths: {
    root: coreWkdir,
    sources: `${coreWkdir}/contracts`,
    artifacts: `${coreWkdir}/artifacts`,
    cache: `${coreWkdir}/cache`,
    tests: `${packageWkdir}/test`
  }
};

module.exports = getHardhatConfig(configOverride);
