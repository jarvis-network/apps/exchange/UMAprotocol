for f in (git ls-files | grep -v 'yarn.lock' | grep -v 'README' | grep -v 'CHANGELOG.md' | grep -v 'package.json')
    test -L $f || sed -i 's/"@uma\//"@jarvis-network\/uma-/g' "$f"
end
