#!/usr/bin/env bash

set -euo pipefail

yarn version --new-version patch --no-git-tag-version
VERSION="$(jq -r '.version' package.json)"
yarn lerna version "$VERSION" --no-git-tag-version --no-push --yes
